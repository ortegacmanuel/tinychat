Class {
	#name : #TCConsole,
	#superclass : #SpPresenter,
	#instVars : [
		'chat',
		'list',
		'input'
	],
	#category : #'TinyChat-Client'
}

{ #category : #'as yet unclassified' }
TCConsole class >> attach: aTinyChat [

	| window |
	window := self on: aTinyChat.
	window open whenClosedDo: [ aTinyChat disconnect ].
	^ window
]

{ #category : #accessing }
TCConsole >> chat: anObject [

	chat := anObject
]

{ #category : #layout }
TCConsole >> defaultLayout [

	^ SpBoxLayout newTopToBottom add: #list; add: #input height: 30; yourself
]

{ #category : #initialization }
TCConsole >> initializePresenters [

	list := self newList.
	input := self newTextInput
		placeholder: 'Type your message here...';
		enabled: true;
		whenSubmitDo: [ :string |
			chat send: string.
			input text: '' ].
	
	self focusOrder add: input.
]

{ #category : #accessing }
TCConsole >> input [

	^ input
]

{ #category : #accessing }
TCConsole >> list [

	^ list
]

{ #category : #initialization }
TCConsole >> print: aCollectionOfMessages [

	list items: (aCollectionOfMessages collect: [ :m | m printString ])
]

{ #category : #initialization }
TCConsole >> setModelBeforeInitialization: aModel [

	chat := aModel
]
